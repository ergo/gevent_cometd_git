from gevent import monkey, pywsgi; monkey.patch_all()
import datetime
import gevent
import gevent_cometd.app_views as app_views
import logging
import optparse
import ConfigParser

from geventwebsocket.handler import WebSocketHandler
from gevent_cometd import config
from webob import Request, exc


log = logging.getLogger(__name__)


class WSGIRouter(object):

    def __call__(self, environ, start_response):
        """ try to find the right request"""
        req = Request(environ)

        # pop the / from path
        query_parts = req.path[1:].split('/')
        action = query_parts[0]
        if not action:
            action = 'index'

        # try to find the action
        try:
            method = getattr(app_views, action)
        except AttributeError:
            raise exc.HTTPBadRequest('No such action %r' % action)

        try:
            res = method(req, *query_parts[1:])
        except exc.HTTPException, e:
            # The exception object itself is a WSGI application/response:
            res = e

        # return proper response
        return res(environ, start_response)

def cli_start():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--ini", dest="ini",
                  help="config file location",
                  default=None
                  )
    parser.add_option("-s", "--secret", dest="secret",
                  help="secret used to secure your requests",
                  default='secret'
                  )
    parser.add_option("-a", "--admin_secret", dest="admin_secret",
                  help="secret used to secure your admin_panel",
                  default='admin_secret'
                  )
    parser.add_option("-p", "--port", dest="port",
                  help="port on which the server listens to",
                  default=8088
                  )
    parser.add_option("-x", "--allowed_post_ip", dest="allow_posting_from",
                  help="comma separated list of ip's that can post to server",
                  default="127.0.0.1"
                  )
    (options, args) = parser.parse_args()
    if options.ini:
        parser = ConfigParser.ConfigParser()
        parser.read(options.ini)
        config['port'] = parser.getint('gevent_cometd', 'port')
        config['secret'] = parser.get('gevent_cometd', 'secret')
        config['admin_secret'] = parser.get('gevent_cometd', 'admin_secret')
        ips = [ip.strip() for ip in parser.get('gevent_cometd',
                                               'allow_posting_from').split(',')]
        config['allow_posting_from'].extend(ips)
    else:
        config['port'] = int(options.port)
        config['secret'] = options.secret
        config['admin_secret'] = options.admin_secret
        config['allow_posting_from'].extend([ip.strip() for ip in options.allow_posting_from.split(',')])

    logging.getLogger().setLevel(logging.INFO)
    print 'Serving on 0.0.0.0:%s' % config['port']
    app = WSGIRouter()
    pywsgi.WSGIServer(('', config['port']), app, log=None,
                      handler_class=WebSocketHandler).serve_forever()
