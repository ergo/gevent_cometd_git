define([ 'demo/client', 'dojo/dom-form', 'dojo/request', 'dojo/on',
        'dojo/query', 'dojo/topic', 'dojo/dom-construct' ], function(
DemoClient, DomForm, Request, On, Query, Topic, DomConstruct) {

    var start = function(webapp_url, server_url, connection_request) {

        var messages_node = Query('.messages')[0];

        Query('#msg_form').on('submit', function(evt) {
            evt.preventDefault();
            Request.post(this.action, {
                data : DomForm.toJson(this),
                handleAs : "text"
            });
            Query('input[name=message]')[0].value = '';
            console.log('sent message');
        });

        var on_connect_callback = function(state) {
            var payload = JSON.stringify({
                conn_id : state.conn_id,
                channels : [ 'test_channel' ]
            });

            Request.post(webapp_url + "/subscribe", {
                handleAs : 'json',
                data : payload,
                headers : {
                    "Content-Type" : "application/json"
                }
            }).then(
            function(data) {
                console.log('subscribed to additional test channel '
                + 'after initial connection');
                console.log(data);
                return true;
            });
        }

        var channel_message_callback = function(entry) {
            var li = DomConstruct.create('li', null, messages_node);
            DomConstruct.create('strong', {
                innerHTML : 'channel:' + entry.channel + ' '
            }, li);
            DomConstruct.create('strong', {
                innerHTML : 'user: ' + entry.user + ' '
            }, li);
            DomConstruct.create('em', {
                innerHTML : entry.message
            }, li);
        }

        var presence_callback = function(entry) {
            console.log('received presence message !');
            var li = DomConstruct.create('li', null, messages_node);
            DomConstruct.create('em', {
                innerHTML : 'user:' + entry.user + ' joined channel: '
                + entry.channel
            }, li);
        }

        DemoClient.connect(webapp_url, server_url, connection_request,
        on_connect_callback);
        Topic.subscribe("gevent_cometd/message", channel_message_callback)
        Topic.subscribe("gevent_cometd/join", presence_callback)

    }

    return {
        start : start
    };
});