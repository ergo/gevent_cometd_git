define([ "dojo/request", 'dojo/json', 'dojo/query', 'dojo/_base/unload',
        'dojo/topic', "dojo/request/script", "dojox/socket",
        "dojox/io/xhrPlugins" ], function(Request, JSON, Query, Unload, Topic,
JSONP, Socket, xhrPlugins) {
    xhrPlugins.addCrossSiteXhr("");
    var state = {
        webapp_url : null,
        server_url : null,
        conn_id : null,
        socket : null,
        connection_request : null,
        on_connect_callback : null,
        reconnect_timout : null,
        check_connection : null,
        last_message : Date.now()
    };

    var jsonp_call = function(args, message) {
        // The parameters to pass to xhrGet, the url, how to handle it, and the
        // callbacks.
        var socket_url = state.server_url + "/listen?conn_id=" + state.conn_id;
        var jsonpArgs = {
            url : socket_url,
            callbackParamName : "callback",
            content : {}
        };
        JSONP.get(jsonpArgs);
    };

    var get_conn_id = function(create_new_socket) {
        if (state.socket) {
            if (state.socket.readyState == 0) {
                // its trying to connect right now
                return false;
            }
            state.socket.close();
        }
        Request.post(state.webapp_url + "/connect", {
            handleAs : 'json',
            data : JSON.stringify(state.connection_request),
            headers : {
                "Content-Type" : "application/json"
            }
        }).then(function(data) {
            state.conn_id = data.conn_id;
            if (create_new_socket) {
                create_socket();
            }
        });
    }

    var message_event = function(event) {
        var data = JSON.parse(event.data);
        console.log('got event', data);
        state.last_message = Date.now();
        for ( var i = 0; i < data.length; i++) {
            console.log('publishing: ' + 'gevent_cometd/' + data[i].type);
            Topic.publish('gevent_cometd/' + data[i].type, data[i]);
        }
    }

    var close_event = function(event) {
        console.log('ws closed');
    };

    var error_event = function(event) {
        console.log('ws error', event);
    };

    var create_socket = function() {
        state.last_message = Date.now();
        console.log('attempting to create socket');
        if (state.check_connection) {
            clearInterval(state.check_connection);
        }

        var socket_url = state.server_url + "/listen?conn_id=" + state.conn_id;

        var ws_capable = false;
        if ("WebSocket" in window) {
            ws_capable = true;
        }
        ws_capable = false;
        if (ws_capable) {
            // "true websocket/ flash path"
            socket_url = socket_url.replace('https', 'wss');
            socket_url = socket_url.replace('http', 'ws');
            state.socket = new WebSocket(socket_url);
            state.socket.onmessage = message_event;
            state.socket.onclose = close_event;
            state.socket.onerror = error_event;
        } else {
            var socket_conf = {
                url : socket_url,
                handleAs : 'text',
                headers : {
                    "Content-Type" : "application/json"
                }
            }
            if (dojo.isOpera || (dojo.isIE && dojo.isIE < 9)) {
                socket_conf.transport = jsonp_call;
            }
            state.socket = dojox.socket.LongPoll(socket_conf);
            state.socket.on("message", message_event);
            state.socket.on("close", close_event);
            state.socket.on("error", error_event);
        }

        state.check_connection = setInterval(function() {
            var last_update = (Date.now() - state.last_message) / 1000.0;
            if (last_update > 20 || state.socket.readyState == 3
            || state.socket.readyState == 2) {
                console.log('reconnect');
                get_conn_id(true);
            }
        }, 3000);

        if (state.on_connect_callback) {
            state.on_connect_callback(state)
        }
    }
    var connect = function(webapp_url, server_url, connection_request,
    on_connect_callback) {
        state.connection_request = connection_request;
        state.webapp_url = webapp_url;
        state.server_url = server_url;
        state.on_connect_callback = on_connect_callback;
        get_conn_id(true);
    }
    /* marks connections for GC */
    Unload.addOnUnload(function() {
        state.socket.close();
        if (state.check_connection) {
            clearInterval(state.check_connection);
        }
        Request.post(state.server_url + "/disconnect", {
            handleAs : 'json',
            data : JSON.stringify({
                'conn_id' : state.conn_id,
            }),
            headers : {
                "Content-Type" : "application/json"
            }
        });
    });
    return {
        connect : connect,
        state : state
    }
});