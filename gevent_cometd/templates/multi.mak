<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<script
    src="http://ajax.googleapis.com/ajax/libs/dojo/1.6.1/dojo/dojo.xd.js"
    type="text/javascript"></script>
<body>

<style type="text/css">
.iframe{
width: 45%;
height: 33%;
border:1px solid;
margin:5px;
}

</style>

<iframe class="iframe" src="/static/iframe_demo.html"></iframe>
<iframe class="iframe" src="/static/iframe_demo.html"></iframe>
<iframe class="iframe" src="/static/iframe_demo.html"></iframe>
<iframe class="iframe" src="/static/iframe_demo.html"></iframe>

<form action="/message" id="msg_form" method="post">
<p>
Public channel<input type="radio" name="channel" value="pub_chan" checked="checked">
Alt. Public channel<input type="radio" name="channel" value="pub_chan2">
</p>
<p>
user <input type="text" name="user" value="User">
</p>
<p>
<textarea name="message" style="width:300px"></textarea>
</p>
<p>
<input type="submit" value="send message">
</p>
</form>

<script type="text/javascript">
dojo.addOnLoad(function() {
    var form = dojo.byId("msg_form");
    dojo.connect(form, "onsubmit", function(event) {
        dojo.stopEvent(event);
        var req = dojo.formToObject(form);
        var xhrArgs = {
            url : form.action,
            postData: dojo.toJson(req, true),
            handleAs : "text",
            load : function(data) {
            },
            error : function(error) {
                console.log(error);
            }
        }
        dojo.query('textarea[name=message]')[0].value = '';
        var deferred = dojo.xhrPost(xhrArgs);
    });
});
</script>

</body>
</html>
