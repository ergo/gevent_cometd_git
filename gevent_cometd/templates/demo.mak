<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Chat demo</title>
<script type="text/javascript" src="/static/swfobject.js"></script>
<script type="text/javascript" src="/static/websocket.js"></script>

% if request.params.get('dojo'):
	<script type="text/javascript">
	var dojoConfig = {
	baseUrl: "/static/",
	packages: [
	    { name: "dojo", location: "//ajax.googleapis.com/ajax/libs/dojo/1.8.1/dojo/" },
	    { name: "dijit", location: "//ajax.googleapis.com/ajax/libs/dojo/1.8.1/dijit/" },
	    { name: "dojox", location: "//ajax.googleapis.com/ajax/libs/dojo/1.8.1/dojox/" },
	    { name: "demo", location: "./"}
	]
	};
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/dojo/1.8.1/dojo/dojo.js" data-dojo-config="async: true"></script>
% else:
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
%endif

</head>
<style type="text/css">
body{
    font-size: 10px;
    font-family: sans;
}
</style>
<body>
	<ul class="messages"></ul>
	
<form action="/message" id="msg_form" method="post">
<p>
Public channel<input type="radio" name="channel" value="pub_chan" checked="checked">
Alt. Public channel<input type="radio" name="channel" value="pub_chan2">
</p>
<p>
user <input type="text" name="user" value="User">
</p>
<p>
<input type="text" name="message" style="width:300px"/>
</p>
<p>
<input type="submit" value="send message">
</p>
</form>
% if request.params.get('dojo'):
<script type="text/javascript">
require(["demo"],function(Demo){
    Demo.start('${config['webapp_url']}',
    '${config['cometd.server']['server']}',
    {'user' : Math.random(),
        'channels' : [ 'pub_chan', 'pub_chan2' ]
    });
});
</script>
%else:
<script src="${config['webapp_url']}/static/jq_client.js"></script>
<script>
WEB_SOCKET_SWF_LOCATION = "${config['webapp_url']}/static/WebSocketMain.swf";
demo_start('${config['webapp_url']}',
    '${config['cometd.server']['server']}',
    {'user' : Math.random(),
        'channels' : [ 'pub_chan', 'pub_chan2' ]
    })
</script>
%endif


</body>
</html>
