<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dumb admin ;-)</title>
<script
    src="http://ajax.googleapis.com/ajax/libs/dojo/1.6.1/dojo/dojo.xd.js"
    type="text/javascript"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/dojo/1.6.1/dijit/themes/claro/claro.css" />
</head>
<body class="claro">

<h1>Unique users remembered: ${len(users)}</h1>
<h1>Unique users connected: ${len([user for user in users.values() if user.connections])}</h1>
<h1>Total connections: ${sum([len(user.connections) for user in users.values()])}</h1>
<h1>Uptime: ${uptime}</h1>
<h1>Messages since start: ${stats['total_unique_messages']}</h1>
<h1>All frames sent: ${stats['total_messages']}</h1>

<h2>Channels</h2>
<ul>
% for channel in channels.values():
<li><strong>${channel.name}</strong> - ${len(channel.connections.keys())} users subscribed<br/>
<h4>Config</h4>
<ul>
<li><strong>Name</strong> --> ${channel.name}</li>
<li><strong>Long Name</strong> --> ${channel.long_name}</li>
<li><strong>Presence</strong> --> ${channel.presence}</li>
<li><strong>Salvagable</strong> --> ${channel.salvagable}</li>
<li><strong>Store History</strong> --> ${channel.store_history}</li>
<li><strong>History Size</strong> --> ${channel.history_size}</li>
</ul>

<h4>Connections</h4>
<ul>
% for user, conns in sorted(channel.connections.items(),key=lambda x:x[0]):
<li><strong>${user}</strong> : ${len(conns)} connections</li>
% endfor
</ul>
</li>
<br/>
% endfor
</ul>

</body>
</html>