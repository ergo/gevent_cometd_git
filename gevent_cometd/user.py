import gevent_cometd
import datetime
import gevent
import gevent.queue
import logging
import uuid
import json

log = logging.getLogger(__name__)


class User(object):
    """ represents a unique user of the system """

    def __init__(self, user, status):
        self.user = user
        self.status = status
        self.connections = []  # holds ids of connections
        self.channels = []  # holds ids of channels
        self.last_active = datetime.datetime.utcnow()

    # TODO currently broken
    def notify_presence(self, user, status):
        # notify channel new user connected
        # (if there are no other connections of user present in this channel)
        send_join_msg = True
        for channel in self.channels:
            if channel.presence:
                for connection in self.connections:
                    if connection in channel.connections:
                        send_join_msg = False
                        break
            if send_join_msg:
                gevent_cometd.stats['total_unique_messages'] += 1
                message = {'user': self.user,
                   'message': None,
                   'type': 'join',
                   'timestamp': datetime.datetime.utcnow()
                   }
                # todo presence
                self.add_message(message, exclude_user=connection.user)

    @classmethod
    def by_name(cls, user_name):
        with gevent_cometd.users_lock:
            return gevent_cometd.users.get(user_name)

    @classmethod
    def add_connection(cls, user_name, conn_id,
                       status=gevent_cometd.status_codes['online']):
        """ creates a new connection for user, creates new user if needed """
        with gevent_cometd.users_lock:
            user_inst = gevent_cometd.users.get(user_name)
            if not user_inst:
                user_inst = User(user_name, status)
                gevent_cometd.users[user_name] = user_inst
            connection = Connection(user_name, conn_id)
            gevent_cometd.connections[connection.id] = connection
            user_inst.connections.append(connection.id)
            # mark active
            user_inst.last_active = datetime.datetime.utcnow()
            return user_inst, connection

    def add_message(self, message):
        # mark active
        self.last_active = datetime.datetime.utcnow()
        conns = self.connections[:]
        for conn_id in conns:
            connection = Connection.by_id(conn_id)
            if connection:
                connection.add_message(message)
        del conns


    def __repr__(self):
        return '<User:%s, status:%s, connections:%s>' % (self.user, self.status, len(self.connections))

class Connection(object):
    """ Represents a browser connection"""
    def __init__(self, user, conn_id=None):
        self.user = user  # hold user id/name of connection
        self.last_active = datetime.datetime.utcnow()
        self.id = conn_id or unicode(uuid.uuid4())
        self.queue = gevent.queue.Queue()

    def __repr__(self):
        return '<Connection: id:%s, owner:%s>' % (self.id, self.user)

    def add_message(self, message, stats=None):
        import gevent_cometd.util as util
        gevent_cometd.stats['total_messages'] += 1
        self.last_update = datetime.datetime.utcnow()
        self.queue.put(message)

    @classmethod
    def by_id(cls, conn_id):
        """ "get connection with this id """
        return gevent_cometd.connections.get(conn_id)

    @classmethod
    def gc_pass(cls):
        # collected is user_id:conn_list
        collected = {}
        curr_time = datetime.datetime.utcnow()
        for key, connection in gevent_cometd.connections.items():
            delta = curr_time - connection.last_active
            if delta.seconds >= gevent_cometd.config['gc_conns_after']:
                conn = gevent_cometd.connections.pop(key)
                # wake up connection
                
                if hasattr(conn, 'websocket'):
                    try:
                        conn.websocket.close()
                    except KeyboardInterrupt as e:
                        raise
                    except Exception as e:
                        pass
                # delete conn references from users
                user = User.by_name(conn.user)
                if user:
                    user.connections.remove(key)
                if conn.user in collected:
                    collected[conn.user].append(key)
                else:
                    collected[conn.user] = [key]
        return collected

    def mark_for_gc(self):
        # set last active time for connection 1 hour in past for GC
        self.last_active -= datetime.timedelta(minutes=60)
