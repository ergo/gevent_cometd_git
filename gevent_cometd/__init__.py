from gevent import monkey; monkey.patch_all()
import gevent
import datetime
import pkg_resources

try:
    import gevent.lock as g_lock
except ImportError as e:
    import gevent.coros as g_lock

# not used yet - if ever
#import redis
#redis_conn = redis.Redis('127.0.0.1', 6379, 2)

config = {
          'secret':'',
          'admin_secret':'',
          'gc_conns_after':30,
          'gc_channels_after': 3600 * 72,
          'wake_connections_after':7,
          'allow_posting_from':[],
          'port':8088,
          'webapp_port':5000
          }


stats = {'total_messages':0,
         'total_unique_messages':0,
         'started_on':datetime.datetime.utcnow()}

users_lock = g_lock.RLock()
channels_lock = g_lock.RLock()
channels = {}
users = {}
connections = {}

status_codes = {
                "offline":0,
                "online":1,
                "away":2,
                "hidden":3
                }



# god this is ugly but i dont have time for this now ;-) not important in grand
# scheme of things
statics = {'statics':{}, 'templates':{}}

#TODO: this is broken implementation - have to fix it one day properly
for res in pkg_resources.resource_listdir('gevent_cometd', 'static'):
    if '__init__' not in res:
        statics['statics'][res] = pkg_resources.resource_string('gevent_cometd.static', res)

for res in pkg_resources.resource_listdir('gevent_cometd', 'templates'):
    if '__init__' not in res:
        statics['templates'][res] = pkg_resources.resource_string('gevent_cometd.templates', res)
