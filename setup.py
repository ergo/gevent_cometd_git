import os
import sys
from setuptools import setup, find_packages


setup(name='gevent_cometd',
      version='0.3.1',
      description='Sample cometd server demo',
      classifiers=[
          'Intended Audience :: Developers'
          ],
      author='Marcin Lulek',
      author_email='info@webreactor.eu',
      license='unknown',
      zip_safe=True,
      packages=find_packages(),
      include_package_data=True,
      package_data={
          '': ['*.txt', '*.rst', '*.ini', '*.mak'],
          'gevent_cometd': ['templates/*.mak'],
      },
      install_requires=[
          'gevent',
          'gevent-websocket',
          'mako',
          'webob'
      ],
      entry_points="""
      [console_scripts]
      gevent_cometd = gevent_cometd.start:cli_start
      gevent_cometd_webapp = gevent_cometd.webapp.start:start_app
      """,
      )
